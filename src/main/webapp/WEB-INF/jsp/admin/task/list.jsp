<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet"
	type="text/css" />
<title>${listType }执行任务计划列表</title>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a>
<br>
<table>
	<tr>
		<td colspan="8" class="tableHeadBg">
			<div align="center">${listType }执行任务计划列表</div>
		</td>
	</tr>
	<tr class="tableTitleBg">
		<td width="5%">任务计划ID</td>
		<td width="20%">网站</td>
		<td width="15%">运行IP</td>
		<td width="10%">抓取URL数量</td>
		<td width="10%">最小线程/最大线程/队列长度</td>
		<td width="25%">运行表达式</td>
		<td width="15%" colspan="2">操作</td>
	</tr>
	<c:forEach items="${tasks}" var="task" varStatus="status" >
	<tr class="<c:if test="${status.count%2==0}">tableAlternationBg2</c:if><c:if test="${status.count%2==1}">tableAlternationBg1</c:if>">
		<td>${task.id}</td>
		<td>${task.searchEngineName}</td>
		<td>${task.runServerIp}</td>
		<td>${task.findUrlSize}</td>
		<td>${task.minThreadNum}/${task.maxThreadNum}/${task.poolQueueSize}</td>
		<td>${task.cronExp}</td>
		<td><a href="${pageContext.request.contextPath}/admin/task/modify/${task.id }">修改</a></td>
		<td><a href="${pageContext.request.contextPath}/admin/task/delete/${task.id }">删除</a></td>
	</tr>
	</c:forEach>
</table>
<br>
<a href="<c:url value="/index.jsp" />">返回首页</a>
<br>
</body>
</html>
