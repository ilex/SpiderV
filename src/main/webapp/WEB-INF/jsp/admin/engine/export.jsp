<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<script src="<c:url value="/js/jquery-1.4.2.min.js"/>" type="text/javascript"></script>
<title>从已经配置成功的搜索引擎导出配置(XML)</title>
<script type="text/javascript">
	$(document).ready(function(){
		$("#exportBtn").bind("click",function(){
			var arr = new Array(); 
			var options=$("#engine_select option:selected");
			options.each(function(i){
				arr[i]=$(this).val();
			 }); 
			if(arr.length > 0){
				var engineIds=arr.join("_");
				var url='<c:url value="/admin/engine/export/"/>'+engineIds;
				window.location.href=url;
			}else{
				alert("请选择需要导出的搜索引擎!");
			}
		});
	}); 
</script>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a><br>
	<table>
		<tr>
			<td colspan="3" class="tableHeadBg">
				<div align="center">从已经配置成功的搜索引擎导出配置(XML)</div>
			</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel" width="20%">需要到出的搜索引擎:</td>
			<td width="40%">
				<select id="engine_select"  multiple="multiple" size="50">
					<c:forEach var="engine" items="${engines}">
						<option value="${engine.id}">${engine.name}</option>
					</c:forEach>
				</select>
			</td>
			<td width="40%">&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td>&nbsp;</td>
			<td>
				<input id="exportBtn" type="button"  value="导出">
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<br><a href="<c:url value="/index.jsp" />">返回首页</a><br>
</body>
</html>
