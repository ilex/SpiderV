package com.yzq.os.spider.v.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;

import com.yzq.os.spider.v.service.domain.UserService;

/**
 * 登陆控制器
 * 
 * @author 苑志强(xingyu_yzq@163.com)
 * 
 */
@Controller
public class LoginController extends CommonController {

	@Autowired
	private UserService userService;

	/**
	 * 用户登陆
	 * 
	 * @param username
	 * @param password
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public RedirectView login(String username, String password,
			HttpSession session) {
		if (userService.login(username, password, session)) {
			return new RedirectView("../index.jsp");
		} else {
			return new RedirectView("../login.jsp");
		}
	}

	/**
	 * 用户退出
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/logout")
	public RedirectView logout(HttpSession session) {
		userService.logout(session);
		return new RedirectView("../login.jsp");
	}

}
