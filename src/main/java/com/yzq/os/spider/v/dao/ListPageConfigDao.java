package com.yzq.os.spider.v.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import com.yzq.os.spider.v.dao.mapper.ListPageConfigMapper;
import com.yzq.os.spider.v.domain.ListPageConfig;

@Repository
public class ListPageConfigDao extends AbstractDao {

	public void save(ListPageConfig config) {
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into list_page_config ");
		sql.append("   (search_engine_id, ");
		sql.append("    no_data_page_regex, ");
		sql.append("    return_record_num_regex, ");
		sql.append("    current_page_no_name, ");
		sql.append("    page_size, ");
		sql.append("    max_record_num, ");
		sql.append("    data_region_regex, ");
		sql.append("    job_title_regex, ");
		sql.append("    job_href_regex, ");
		sql.append("    job_date_regex, ");
		sql.append("    job_date_pattern, ");
		sql.append("    job_city_regex, ");
		sql.append("    job_type_name, ");
		sql.append("    job_industry_name, ");
		sql.append("    job_city_name, ");
		sql.append("    company_name_regex, ");
		sql.append("    company_href_regex) ");
		sql.append(" values ");
		sql.append("   (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");

		List<Object> params = new ArrayList<Object>();
		params.add(config.getSearchEngineId());
		params.add(config.getNoDataPageRegex());
		params.add(config.getReturnRecordNumRegex());
		params.add(config.getCurrentPageNoName());
		params.add(config.getPageSize());
		params.add(config.getMaxRecordNum());
		params.add(config.getDataRegionRegex());
		params.add(config.getJobTitleRegex());
		params.add(config.getJobHrefRegex());
		params.add(config.getJobDateRegex());
		params.add(config.getJobDatePattern());
		params.add(config.getJobCityRegex());
		params.add(config.getJobTypeName());
		params.add(config.getIndustryName());
		params.add(config.getCityName());
		params.add(config.getCompanyNameRegex());
		params.add(config.getCompanyHrefRegex());

		jdbcTemplateOnline.update(sql.toString(), params.toArray());
	}

	public List<ListPageConfig> findAll() {
		StringBuffer sql = new StringBuffer();
		sql.append(" select t.id, ");
		sql.append("        se.site_id, ");
		sql.append("        t.search_engine_id, ");
		sql.append("        se.name as search_engine_name, ");
		sql.append("        t.no_data_page_regex, ");
		sql.append("        t.return_record_num_regex, ");
		sql.append("        t.current_page_no_name, ");
		sql.append("        t.page_size, ");
		sql.append("        t.max_record_num, ");
		sql.append("        t.data_region_regex, ");
		sql.append("        t.job_title_regex, ");
		sql.append("        t.job_href_regex, ");
		sql.append("        t.job_date_regex, ");
		sql.append("        t.job_date_pattern, ");
		sql.append("        t.job_city_regex, ");
		sql.append("        t.job_type_name, ");
		sql.append("        t.job_industry_name, ");
		sql.append("        t.job_city_name, ");
		sql.append("        t.company_name_regex, ");
		sql.append("        t.company_href_regex ");
		sql.append("   from list_page_config t, search_engine se ");
		sql.append("  where t.search_engine_id = se.id ");

		return jdbcTemplateOnline.query(sql.toString(), new ListPageConfigMapper());
	}

	public ListPageConfig findById(int id) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select t.id, ");
		sql.append("        t.search_engine_id, ");
		sql.append("        t.no_data_page_regex, ");
		sql.append("        t.return_record_num_regex, ");
		sql.append("        t.current_page_no_name, ");
		sql.append("        t.page_size, ");
		sql.append("        t.max_record_num, ");
		sql.append("        t.data_region_regex, ");
		sql.append("        t.job_title_regex, ");
		sql.append("        t.job_href_regex, ");
		sql.append("        t.job_date_regex, ");
		sql.append("        t.job_date_pattern, ");
		sql.append("        t.job_city_regex, ");
		sql.append("        t.job_type_name, ");
		sql.append("        t.job_industry_name, ");
		sql.append("        t.job_city_name, ");
		sql.append("        t.company_name_regex, ");
		sql.append("        t.company_href_regex ");
		sql.append("   from list_page_config t ");
		sql.append("  where t.id = ? ");

		return jdbcTemplateOnline.queryForObject(sql.toString(), new Object[] { id }, new ListPageConfigMapper());
	}

	public ListPageConfig findBySearchEngineId(int searchEngineId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select t.id, ");
		sql.append("        t.search_engine_id, ");
		sql.append("        t.no_data_page_regex, ");
		sql.append("        t.return_record_num_regex, ");
		sql.append("        t.current_page_no_name, ");
		sql.append("        t.page_size, ");
		sql.append("        t.max_record_num, ");
		sql.append("        t.data_region_regex, ");
		sql.append("        t.job_title_regex, ");
		sql.append("        t.job_href_regex, ");
		sql.append("        t.job_date_regex, ");
		sql.append("        t.job_date_pattern, ");
		sql.append("        t.job_city_regex, ");
		sql.append("        t.job_type_name, ");
		sql.append("        t.job_industry_name, ");
		sql.append("        t.job_city_name, ");
		sql.append("        t.company_name_regex, ");
		sql.append("        t.company_href_regex ");
		sql.append("   from list_page_config t ");
		sql.append("  where t.search_engine_id = ? ");

		List<ListPageConfig> configs = jdbcTemplateOnline.query(sql.toString(), new Object[] { searchEngineId }, new ListPageConfigMapper());

		if (CollectionUtils.isNotEmpty(configs)) {
			return configs.get(0);
		} else {
			return null;
		}
	}

	public void update(ListPageConfig config) {
		StringBuffer sql = new StringBuffer();
		sql.append(" update list_page_config t ");
		sql.append("    set t.search_engine_id        = ?, ");
		sql.append("        t.no_data_page_regex      = ?, ");
		sql.append("        t.return_record_num_regex = ?, ");
		sql.append("        t.current_page_no_name    = ?, ");
		sql.append("        t.page_size               = ?, ");
		sql.append("        t.max_record_num          = ?, ");
		sql.append("        t.data_region_regex       = ?, ");
		sql.append("        t.job_title_regex         = ?, ");
		sql.append("        t.job_href_regex          = ?, ");
		sql.append("        t.job_date_regex          = ?, ");
		sql.append("        t.job_date_pattern        = ?, ");
		sql.append("        t.job_city_regex          = ?, ");
		sql.append("        t.job_type_name           = ?, ");
		sql.append("        t.job_industry_name       = ?, ");
		sql.append("        t.job_city_name           = ?, ");
		sql.append("        t.company_name_regex      = ?, ");
		sql.append("        t.company_href_regex      = ? ");
		sql.append("  where t.id = ? ");

		List<Object> params = new ArrayList<Object>();
		params.add(config.getSearchEngineId());
		params.add(config.getNoDataPageRegex());
		params.add(config.getReturnRecordNumRegex());
		params.add(config.getCurrentPageNoName());
		params.add(config.getPageSize());
		params.add(config.getMaxRecordNum());
		params.add(config.getDataRegionRegex());
		params.add(config.getJobTitleRegex());
		params.add(config.getJobHrefRegex());
		params.add(config.getJobDateRegex());
		params.add(config.getJobDatePattern());
		params.add(config.getJobCityRegex());
		params.add(config.getJobTypeName());
		params.add(config.getIndustryName());
		params.add(config.getCityName());
		params.add(config.getCompanyNameRegex());
		params.add(config.getCompanyHrefRegex());
		params.add(config.getId());

		jdbcTemplateOnline.update(sql.toString(), params.toArray());

	}

	public void deleteBySearchEngineId(Integer searchEngineId) {
		String sql = " delete from list_page_config where search_engine_id = ? ";
		jdbcTemplateOnline.update(sql, searchEngineId);
	}
}
