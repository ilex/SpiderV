package com.yzq.os.spider.v.service.http;

import java.net.URI;

import org.apache.http.ProtocolException;
import org.apache.http.impl.client.DefaultRedirectStrategy;

import com.yzq.os.spider.v.util.EncodeUtil;

/**
 * 重写此方法
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public class UTFRedirectStrategy extends DefaultRedirectStrategy {


	@Override
	protected URI createLocationURI(String url) throws ProtocolException {
		url = EncodeUtil.iso2utf(url);
		return super.createLocationURI(url);
	}

}
