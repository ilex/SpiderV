package com.yzq.os.spider.v.util;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import com.yzq.os.spider.v.Constants;

/**
 * 正则匹配结果集
 * 
 * @author 苑志强(xingyu_yzq@163.com)
 * 
 */
public class ResultSet implements Serializable, Cloneable {

	private static Logger logger = Logger.getLogger(ResultSet.class);

	private static final long serialVersionUID = -74780075322970875L;

	private List<Object> infolist = new ArrayList<Object>();

	/**
	 * 添加结果
	 * 
	 * @param obj
	 */
	public void add(Object obj) {
		infolist.add(obj);
	}

	/**
	 * 获取指定下标的字符串结果
	 * 
	 * @param index
	 * @return
	 */
	public String getString(int index) {
		return (String) infolist.get(index);
	}

	/**
	 * 获取指定下标的int结果
	 * 
	 * @param index
	 * @return
	 */
	public int getInt(int index) {
		String str = getString(index);
		if (str != null) {
			try {
				return Integer.parseInt(str);
			} catch (Exception e) {
				logger.error("", e);
			}
		}
		return 0;
	}

	/**
	 * 获取指定下标的long结果
	 * 
	 * @param index
	 * @return
	 */
	public long getLong(int index) {
		String str = getString(index);
		if (str != null) {
			try {
				return Long.parseLong(str);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	/**
	 * 获取指定下标的double结果
	 * 
	 * @param index
	 * @return
	 */
	public double getDouble(int index) {
		String str = getString(index);
		if (str != null) {
			try {
				return Double.parseDouble(str);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	/**
	 * 获取指定下标的日期结果(yyyy-MM-dd,yyyy-MM-dd HH:mm:ss)
	 * 
	 * @param index
	 * @return
	 */
	public Date getDate(int index) {
		String str = getString(index);
		Date date = null;
		try {
			date = DateUtils.parseDate(str, new String[] {
					Constants.DATE_PATTERN, Constants.DATE_WITH_TIME_PATTERN });
		} catch (ParseException e) {
			logger.error("", e);
		}
		return date;
	}

	/**
	 * 获取指定下标的日期结果,格式通过参数设定
	 * 
	 * @param index
	 * @param format
	 * @return
	 */
	public Date getDate(int index, String format) {
		String str = getString(index);

		Date date = null;
		try {
			date = DateUtils.parseDate(str, new String[] { format });
		} catch (ParseException e) {
		}

		return date;
	}

	/**
	 * 获取指定下标的boolean结果
	 * 
	 * @param index
	 * @return
	 */
	public boolean getBoolean(int index) {
		String str = getString(index);
		if (str != null) {
			if ("true".equalsIgnoreCase(str)) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	/**
	 * 获取指定下标的Object结果
	 * 
	 * @param index
	 * @return
	 */
	public Object getObject(int index) {
		return infolist.get(index);
	}

	/**
	 * 返回数据结果数量
	 * 
	 * @return
	 */
	public int size() {
		return infolist.size();
	}

	/**
	 * 返回数据集合
	 * 
	 * @return
	 */
	public List<Object> getList() {
		return infolist;
	}

	/**
	 * 克隆自身
	 * 
	 * @return
	 */
	public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch (CloneNotSupportedException e) {
			logger.error("", e);
		}
		return o;
	}

}