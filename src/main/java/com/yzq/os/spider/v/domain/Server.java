package com.yzq.os.spider.v.domain;

/**
 * 抓取集群服务器
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public class Server {

	private Integer id;
	/**
	 * IP地址
	 */
	private String ip;
	/**
	 * 服务器在集群中的角色，0：普通（只抓取），1：中控（执行一些单点运行的定时任务等）
	 */
	private int type;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Server [id=" + id + ", ip=" + ip + ", type=" + type + "]";
	}

}
