package com.yzq.os.spider.v.service.spider.impl.before;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import com.yzq.os.spider.v.service.spider.AbstractBeforeCrawlProcessor;
import com.yzq.os.spider.v.util.Encode;

/**
 * 抓取前置处理器的例子
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public class DemoBeforeCrawlProcessor extends AbstractBeforeCrawlProcessor {

	private static Logger logger = Logger.getLogger(DemoBeforeCrawlProcessor.class);

	private static final String LOGIN_URL = "http://www.xxx.com/Login.jsp";
	
	private static final List<Header> LOGIN_HEADERS = new ArrayList<Header>();

	private static final List<NameValuePair> LOGIN_PARAMS = new ArrayList<NameValuePair>();
	
	static {
		LOGIN_HEADERS.add(new BasicHeader("Referer", LOGIN_URL));
		
		LOGIN_PARAMS.add(new BasicNameValuePair("UserName", "aaa"));
		LOGIN_PARAMS.add(new BasicNameValuePair("Password", "bbb"));
	}

	@Override
	public boolean process() {
		boolean result = false;
		try {
			httpClientService.doPostRequest(LOGIN_URL, Encode.GB18030, true, LOGIN_HEADERS, LOGIN_PARAMS, "UTF-8");
			result = true;
		} catch (Exception e) {
			logger.error("", e);
		}
		return result;
	}

}
