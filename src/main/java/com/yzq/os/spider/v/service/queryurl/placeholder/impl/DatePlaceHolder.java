package com.yzq.os.spider.v.service.queryurl.placeholder.impl;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

/**
 * 日期占位符
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public class DatePlaceHolder extends AbstractPlaceHolder {

	private static final String PLACE_HOLDER_STRING = "<<yyyy-MM-dd>>";

	public DatePlaceHolder() {
		this.placeHolderString = PLACE_HOLDER_STRING;
	}

	@Override
	public String replace(String value) {
		return StringUtils.replace(value, getEncodedHolderString(), getEncodedHolderValue(DateFormatUtils.format(new Date(), "yyyy-MM-dd")));
	}

	@Override
	public String returnPlaceString() {
		return this.placeHolderString;
	}

}
