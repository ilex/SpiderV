package com.yzq.os.spider.v.service.queryurl.placeholder.impl;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

/**
 * 日期加时间占位符2
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public class DateTimePlaceHolder2 extends AbstractPlaceHolder {

	private static final String PLACE_HOLDER_STRING = "<<yyyy.MM.dd HH:mm:ss>>";

	public DateTimePlaceHolder2() {
		this.placeHolderString = PLACE_HOLDER_STRING;
	}

	@Override
	public String replace(String value) {
		return StringUtils.replace(value, getEncodedHolderString(), getEncodedHolderValue(DateFormatUtils.format(new Date(), "yyyy.MM.dd HH:mm:ss")));
	}

	@Override
	public String returnPlaceString() {
		return this.placeHolderString;
	}

}
