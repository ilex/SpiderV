package com.yzq.os.spider.v.service.queryurl.placeholder;

/**
 * 搜索URL中的占位符替换接口
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public interface PlaceHolder {
	
	void setUrlEncode(String urlEncode);
	
	String replace(String value);
	
	String returnPlaceString();

}
