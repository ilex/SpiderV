package com.yzq.os.spider.v.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.yzq.os.spider.v.dao.mapper.ServerMapper;
import com.yzq.os.spider.v.domain.Server;

@Repository
public class ServerDao extends AbstractDao {
	public List<Server> findAll() {
		String sql = " select id, ip, type from servers order by ip ";
		return jdbcTemplateOnline.query(sql, new ServerMapper());
	}
}
