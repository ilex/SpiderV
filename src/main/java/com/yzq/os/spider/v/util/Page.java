package com.yzq.os.spider.v.util;

import java.util.List;

/**
 * 分页对象
 * 
 * @author 苑志强(xingyu_yzq@163.com)
 * 
 * @param <T>
 */
public class Page<T> implements java.io.Serializable {

	private static final long serialVersionUID = 7530098811944192485L;

	/**
	 * 默认每页显示记录数量
	 */
	public static final int DEFAULT_PAGE_SIZE = 20;

	/**
	 * 每页显示数量
	 */
	private int myPageSize = DEFAULT_PAGE_SIZE;

	/**
	 * 数据库记录起始Index
	 */
	private int start;

	/**
	 * 总记录数
	 */
	private int totalSize;

	/**
	 * 当前页数据集对象
	 */
	private List<T> data;

	/**
	 * 当前页码
	 */
	private int currentPageno;

	/**
	 * 总页数
	 */
	private int totalPageCount;

	public Page(int start, int totalSize, List<T> data) {
		this(start, totalSize, DEFAULT_PAGE_SIZE, data);
	}

	public Page(int start, int totalSize, int pageSize, List<T> data) {
		this.myPageSize = pageSize;
		this.start = start;
		this.totalSize = totalSize;
		this.data = data;
		this.currentPageno = (start - 1) / pageSize + 1;
		this.totalPageCount = (totalSize + pageSize - 1) / pageSize;
		if (totalSize == 0) {
			this.currentPageno = 1;
			this.totalPageCount = 1;
		}
	}

	/**
	 * 获取当前页数据集对象
	 * 
	 * @return
	 */
	public List<T> getData() {
		return this.data;
	}

	/**
	 * 获取本页记录数量
	 * 
	 * @return
	 */
	public int getPageSize() {
		return this.myPageSize;
	}

	/**
	 * 判断是否存在下一页
	 * 
	 * @return
	 */
	public boolean hasNextPage() {
		return (this.getCurrentPageNo() < this.getTotalPageCount());
	}

	/**
	 * 判断是否有上一页
	 * 
	 * @return
	 */
	public boolean hasPreviousPage() {
		return (this.getCurrentPageNo() > 1);
	}

	/**
	 * 获取数据库记录起始Index
	 * 
	 * @return
	 */
	public int getStart() {
		return start;
	}

	/**
	 * 获取数据库记录结束Index
	 * 
	 * @return
	 */
	public int getEnd() {
		int end = this.getStart() + myPageSize - 1;
		if (end < 0) {
			end = 0;
		}
		return end;
	}

	/**
	 * 获取上一页数据库记录开始Index
	 * 
	 * @return
	 */
	public int getStartOfPreviousPage() {
		return Math.max(start - myPageSize, 1);
	}

	/**
	 * 获取下一页数据库记录开始Index
	 * 
	 * @return
	 */
	public int getStartOfNextPage() {
		return start + myPageSize;
	}

	/**
	 * 获取任意页数据库记录开始Index
	 * 
	 * @param pageNo
	 * @return
	 */
	public static int getStartOfAnyPage(int pageNo) {
		return getStartOfAnyPage(pageNo, DEFAULT_PAGE_SIZE);
	}

	/**
	 * 获取任意页数据库记录开始Index（从0开始）
	 * 
	 * @param pageNo
	 * @return
	 */
	public static int getStartOfAnyPageFromZero(int pageNo) {
		return getStartOfAnyPage(pageNo, DEFAULT_PAGE_SIZE) - 1;
	}

	/**
	 * 根据页码和每页显示数量获取数据库记录开始Index（从0开始）
	 * 
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public static int getStartOfAnyPageFromZero(int pageNo, int pageSize) {
		return getStartOfAnyPage(pageNo, pageSize) - 1;
	}

	/**
	 * 根据页码和每页显示数量获取数据库记录开始Index
	 * 
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public static int getStartOfAnyPage(int pageNo, int pageSize) {
		int startIndex = (pageNo - 1) * pageSize + 1;
		if (startIndex < 1)
			startIndex = 1;
		return startIndex;
	}

	/**
	 * 根据记录总数和每页显示数量获取总页数
	 * 
	 * @param totalSize
	 * @param pageSize
	 * @return
	 */
	public static int getAnyTotalPageCount(int totalSize, int pageSize) {
		int totalPageCount = (totalSize + pageSize - 1) / pageSize;
		if (totalSize == 0) {
			totalPageCount = 1;
		}
		return totalPageCount;
	}

	/**
	 * 获取数据记录总数
	 * 
	 * @return
	 */
	public int getTotalSize() {
		return this.totalSize;
	}

	/**
	 * 获取当前页码
	 * 
	 * @return
	 */
	public int getCurrentPageNo() {
		return this.currentPageno;
	}

	/**
	 * 获取总页码数
	 * 
	 * @return
	 */
	public int getTotalPageCount() {
		return this.totalPageCount;
	}

}
