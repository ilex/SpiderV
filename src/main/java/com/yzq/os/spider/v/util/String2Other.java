package com.yzq.os.spider.v.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * 字符串处理类
 * 
 * @author 苑志强(xingyu_yzq@163.com)
 * 
 */
public final class String2Other {

	/**
	 * 将字符串默认split，去掉空的结果
	 * 
	 * @param inputStr
	 * @return
	 */
	public static List<String> defaultSplitList(String inputStr) {
		List<String> returnList = new ArrayList<String>();
		if (StringUtils.isNotBlank(inputStr)) {
			returnList = Arrays.asList(StringUtils.split(inputStr));
		}
		// remove blanks
		Iterator<String> it = returnList.iterator();
		while (it.hasNext()) {
			if (StringUtils.isBlank(it.next())) {
				it.remove();
			}
		}
		return returnList;

	}

	/**
	 * 移除html的tag部分
	 * 
	 * @param html
	 * @return
	 */
	public static String removeHTMLTags(String html) {
		String text = null;
		if (html != null) {
			text = html.replaceAll("<.*?>", "");// 鍘婚櫎HTML鏍囩
		}
		return text;
	}

	/**
	 * 去除掉HTML的&nbsp;标签
	 * 
	 * @param html
	 * @return
	 */
	public static String removeNBSP(String html) {
		String text = null;
		if (html != null) {
			text = html.replaceAll("\\&nbsp;", "");// 鍘婚櫎HTML鏍囩
		}
		return text;
	}

	/**
	 * 去除符号
	 * 
	 * @param content
	 * @return
	 */
	public static String removeSymbol(String content) {
		String result = null;
		if (content != null) {
			result = content.replaceAll("(?i)[^a-zA-Z0-9\u4E00-\u9FA5]", "");
		}
		return result;
	}

}
