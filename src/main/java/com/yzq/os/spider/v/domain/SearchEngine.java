package com.yzq.os.spider.v.domain;

/**
 * 目标网站的搜索引擎配置
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public class SearchEngine {
	/**
	 * GET请求
	 */
	public static final int METHOD_GET = 0;
	/**
	 * POST请求
	 */
	public static final int METHOD_POST = 1;

	
	private Integer id;
/**
 * 网站ID
 */
	private int websiteId;
	/**
	 * 搜索引擎名称
	 */
	private String name;
	/**
	 * 提交类型
	 */
	private int method;
	
	/**
	 * 提交url，url中？前面部分
	 */
	private String baseUrl;
	/**
	 * 网站是否支持GZIP压缩，一般默认是开启的
	 */
	private int isGzip;

	/**
	 * 结果页面编码
	 */
	private int encode;
	
	/**
	 * URL编码
	 */
	private int urlEncode;
	
	/**
	 * 每次抓取后距下一次提交休息间隔
	 */
	private int sleepTime;
	
	/**
	 * 创建搜索URL实现类
	 */
	private String createQueryURLClass;
	
	/**
	 * 抓取前置处理器
	 */
	private String beforeSpiderProcClass;
	
	/**
	 * 执行抓取任务实现类
	 */
	private String spiderTaskClass;
	
	/**
	 * 抓取后对数据整体补全实现类
	 */
	private String completionSpiderClass;
	
	/**
	 * 登陆实现类
	 */
	private String loginClass;
	

	/**
	 * 网站名称
	 */
	private String websiteName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMethod() {
		return method;
	}

	public void setMethod(int method) {
		this.method = method;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public int getIsGzip() {
		return isGzip;
	}

	public void setIsGzip(int isGzip) {
		this.isGzip = isGzip;
	}

	public int getEncode() {
		return encode;
	}

	public void setEncode(int encode) {
		this.encode = encode;
	}

	public int getSleepTime() {
		return sleepTime;
	}

	public void setSleepTime(int sleepTime) {
		this.sleepTime = sleepTime;
	}

	public String getCreateQueryURLClass() {
		return createQueryURLClass;
	}

	public void setCreateQueryURLClass(String createQueryURLClass) {
		this.createQueryURLClass = createQueryURLClass;
	}

	

	

	

	public String getBeforeSpiderProcClass() {
		return beforeSpiderProcClass;
	}

	public void setBeforeSpiderProcClass(String beforeSpiderProcClass) {
		this.beforeSpiderProcClass = beforeSpiderProcClass;
	}

	public String getSpiderTaskClass() {
		return spiderTaskClass;
	}

	public void setSpiderTaskClass(String spiderTaskClass) {
		this.spiderTaskClass = spiderTaskClass;
	}

	public int getWebsiteId() {
		return websiteId;
	}

	public void setWebsiteId(int websiteId) {
		this.websiteId = websiteId;
	}

	public String getWebsiteName() {
		return websiteName;
	}

	public void setWebsiteName(String websiteName) {
		this.websiteName = websiteName;
	}

	

	public String getCompletionSpiderClass() {
		return completionSpiderClass;
	}

	public void setCompletionSpiderClass(String completionSpiderClass) {
		this.completionSpiderClass = completionSpiderClass;
	}

	public String getLoginClass() {
		return loginClass;
	}

	public void setLoginClass(String loginClass) {
		this.loginClass = loginClass;
	}

	

	public int getUrlEncode() {
		return urlEncode;
	}

	public void setUrlEncode(int urlEncode) {
		this.urlEncode = urlEncode;
	}

	

	@Override
	public String toString() {
		return "SearchEngine [id=" + id + ", websiteId=" + websiteId + ", name=" + name + ", method=" + method + ", baseUrl=" + baseUrl + ", isGzip=" + isGzip + ", encode=" + encode + ", urlEncode=" + urlEncode + ", sleepTime=" + sleepTime + ", createQueryURLClass=" + createQueryURLClass + ", beforeSpiderProcClass="
				+ beforeSpiderProcClass + ", spiderTaskClass=" + spiderTaskClass + ", completionSpiderClass=" + completionSpiderClass + ", loginClass=" + loginClass + ",  websiteName=" + websiteName + "]";
	}

}
