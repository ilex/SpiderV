package com.yzq.os.spider.v.service.spider;

import com.yzq.os.spider.v.service.http.HttpClientService;

/**
 * 抓取前置处理器
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public interface BeforeCrawlProcessor {

	void setHttpClientService(HttpClientService httpClientService);
 
	/**
	 * 执行前置处理任务
	 * @return
	 */
	boolean process();
}
