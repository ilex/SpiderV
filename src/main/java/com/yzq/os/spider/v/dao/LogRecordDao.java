package com.yzq.os.spider.v.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.stereotype.Repository;

import com.yzq.os.spider.v.Constants;
import com.yzq.os.spider.v.dao.mapper.LogRecordMapper;
import com.yzq.os.spider.v.domain.LogRecord;

@Repository
public class LogRecordDao extends AbstractDao {

	public void save(LogRecord record) {
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into spider_run_log ");
		sql.append("   (spider_date, ");
		sql.append("    search_engine_id, ");
		sql.append("    ip, ");
		sql.append("    query_count, ");
		sql.append("    query_speed, ");
		sql.append("    record_count, ");
		sql.append("    record_speed, ");
		sql.append("    start_time, ");
		sql.append("    finish_time) ");
		sql.append(" values ");
		sql.append("   ( ?, ?, ?, ?, ?, ?, ?, ?, ? ) ");

		List<Object> paramList = new ArrayList<Object>();
		paramList.add(record.getSpiderDate());
		paramList.add(record.getSearchEngineId());
		paramList.add(record.getIp());
		paramList.add(record.getQueryCount());
		paramList.add(record.getQuerySpeed());
		paramList.add(record.getRecordCount());
		paramList.add(record.getRecordSpeed());
		paramList.add(record.getStartTime());
		paramList.add(record.getFinishTime());

		jdbcTemplateOnline.update(sql.toString(), paramList.toArray());

	}

	public List<LogRecord> findByCrawlDate(Date spiderDate) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select t.id, ");
		sql.append("        t.spider_date, ");
		sql.append("        se.site_id, ");
		sql.append("        t.search_engine_id, ");
		sql.append("        se.name as search_engine_name, ");
		sql.append("        t.ip, ");
		sql.append("        t.query_count, ");
		sql.append("        t.query_speed, ");
		sql.append("        t.record_count, ");
		sql.append("        t.record_speed, ");
		sql.append("        t.start_time, ");
		sql.append("        t.finish_time ");
		sql.append("   from spider_run_log t, search_engine se ");
		sql.append("  where t.search_engine_id = se.id ");
		sql.append("    and t.spider_date = ? ");
		sql.append("  order by t.search_engine_id, t.finish_time ");

		return jdbcTemplateOnline.query(sql.toString(), new Object[] { DateFormatUtils.format(spiderDate, Constants.DATE_PATTERN) }, new LogRecordMapper());
	}

}
