package com.yzq.os.spider.v.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.yzq.os.spider.v.domain.SpiderTask;

public class SpiderTaskMapper implements RowMapper<SpiderTask> {

	@Override
	public SpiderTask mapRow(ResultSet rs, int index) throws SQLException {
		SpiderTask task = new SpiderTask();
		task.setId(rs.getInt("id"));
		task.setRunServerIp(rs.getString("run_server_ip"));
		task.setSearchEngineId(rs.getInt("search_engine_id"));
		try {
			task.setSearchEngineName(rs.getString("search_engine_name"));
		} catch (Exception e) {
		}
		task.setFindUrlSize(rs.getInt("find_url_size"));
		task.setMinThreadNum(rs.getInt("min_thread_num"));
		task.setMaxThreadNum(rs.getInt("max_thread_num"));
		task.setCronExp(rs.getString("cron_exp"));
		task.setPoolQueueSize(rs.getInt("pool_queue_size"));
		return task;
	}

}
